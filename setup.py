from setuptools import setup
import os

setup(name='jaws_sharding',
    version=os.popen('git describe --dirty=-dev --always --tags --abbrev=6').read().strip().replace("-", "+", 1),
    description='Utility for sharding fasta/fastq ref/subject db files',
    url='https://gitlab.com/jfroula/jaws-sharding',
    author='Seung-jin Sul, Jeff Froula',
    packages=['jaws_sharding'],
    install_requires=[line.strip() for line in open("requirements.txt")],
    scripts = [
        'jaws_sharding/create_blocks.py',
        'jaws_sharding/decompress_and_fix_headers.py',
        'jaws_sharding/fasta_indexer.py',
        'jaws_sharding/fastq_indexer.py',
        'jaws_sharding/shard_reader.py',
        'jaws_sharding/textfile_indexer.py'
        ],
    zip_safe=False)
