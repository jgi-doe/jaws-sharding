# Scripts for Sharding
This repo contains script to perform sharding for WDLs using JAWS. Sharding is the act of splitting up fastq, fasta, etc, files so they can be used in parallel jobs.

# What You Can Shard
* fasta files
* fastq files
* tab or space delineated text files

# Docker Image
see [jaws-sharding](https://cloud.docker.com/repository/docker/jfroula/jaws-sharding)

Only one docker image required for sharding
```
* jfroula/jaws-sharding:1.0.8
```

Additional image required for this alignment example:
```
* jfroula/aligner-bbmap:1.0.0
```

# Implementing Sharding in Your WDL 
Please see the `shard_test.wdl` 

# Run the Example
```
java -jar /global/dna/projectdirs/DSI/workflows/cromwell/java/cromwell.jar run shard_test.wdl -i shard.json

```
---------------------------------------------------------
# How to Use Sharding in My Scripts Without Using a WDL
To test with supplied fasta
```
# create index file of the fasta sequence
bin/fasta_indexer.py -i data/DOE_UTEX.polished.t635masked.fasta.gz -o indexes

# create a file of blocks representing start and end points representing fasta# I tried different block sizes. 100000000 gives me 2 blocks
bin/create_blocks.py -if indexes -ff data/DOE_UTEX.polished.t635masked.fasta.gz -of blocks -bs 100000000

# this step grabs a block of sequence from the fasta and pipes it to some script. 
# For example, I'll use wc -c here but things like blastn already handles input as stdin.
# We can write some kind of loop in bash (or whatever)

while read -r block; do
    start=$(echo ${block} | awk '{print $1}')
    end=$(echo ${block} | awk '{print $2}')
    
    # we are piping a block of the fastq sequence to the aligner
    bin/shard_reader.py -i data/DOE_UTEX.polished.t635masked.fasta.gz -s $start -e $end | \
    wc -c
done < blocks
```
** Note that there is a fastq and tabular file sharder as well.  
** You will need to test out different block sizes to get the one that results in an acceptable number of blocks.


