#!/usr/bin/env python
## usage = "python fasta_reader.py -i inFile -s startOffset -e endOffset"
#
# Catting a fasta shard to stdout
#
# ex)
# $ fasta_reader.py -i arctic_10.faa -s 0 -e 1458736
#
# Seung-Jin Sul (ssul@lbl.gov)
#
import argparse
import bz2
import gzip

desc = "shard reader"
parser = argparse.ArgumentParser(description=desc)
parser.add_argument("-i", "--file", dest="infilename", help="query file", required=True)
parser.add_argument("-s", "--start", dest="startOffset", help="start offset", required=True,
                    type=int)
parser.add_argument("-e", "--end", dest="endOffset", help="end offset", required=True, type=int)
options = parser.parse_args()

def writeFile(FH):
    FH.seek(options.startOffset, 0)
    lastPos = 0
    while lastPos <= options.endOffset:
        print(FH.readline().rstrip())
        lastPos = FH.tell()

if options.infilename.endswith(".bz2"):
    with bz2.open(options.infilename, 'rt') as FH:
        writeFile(FH)
elif options.infilename.endswith(".gz"):
    with gzip.open(options.infilename, 'rt') as FH:
        writeFile(FH)
else:
    with open(options.infilename, 'rt') as FH:
        writeFile(FH)
