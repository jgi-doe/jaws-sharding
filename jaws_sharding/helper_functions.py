# Jeff Froula <jlfroula@lbl.gov>
# 08/21/2019
#
# Summary:
# --------
# functions to supplement sharding scripts
#
# Assumptions:
# --------
# Requires python 3
#
import sys,os
from subprocess import Popen, call, PIPE
import bz2
import gzip

def unCompressFile(filename, mode, compressFormat=None, **kw):
    """Open a filename which can be either compressed or plain file.
    @param compressFormat if None, an attempt will be made to autodetect format
    (currently by extension, only '.gz' and '.bz2' are recognized); if "none" - open as plain
    file, if "gzip" - open as gzip file. Decompressed file is written to disk."""
    cform = compressFormat
    new_filename=filename
    if cform is None:
        cform = "none"
        if filename.endswith('.gz'):
            cform = "gzip"
            (new_filename,ext) = os.path.splitext(os.path.basename(filename))
        elif filename.endswith('.bz2'):
            cform = "bz2"
            (new_filename,ext) = os.path.splitext(os.path.basename(filename))
    k = kw.copy()
    if cform == "gzip":
        if "buffering" in k:
            k["bufsize"] = k["buffering"]
            del k["buffering"]
        with gzip.open(filename, 'rb', **kw) as f:
            content = f.read()
        with open(new_filename, "wb") as o:
            o.write(content)
    elif cform == "bz2":
        k.setdefault("buffering",2**20)
        with bz2.open(filename, "rb", **kw) as f:
            # Decompress data from file
            content = f.read()
        with open(new_filename, "wb") as o:
            o.write(content)
    elif cform == "none":
        pass
    else:
        raise ValueError(compressFormat)

    return new_filename

def openCompressed(filename, mode, compressFormat=None, **kw):
    """Open a filename which can be either compressed or plain file and write it to output.
    @param compressFormat if None, an attempt will be made to autodetect format
    (currently by extension, only '.gz' and '.bz2' are recognized); if "none" - open as plain
    file, if "gzip" - open as gzip file."""
    cform = compressFormat
    if cform is None:
        cform = "none"
        if filename.endswith('.gz'):
            cform = "gzip"
        elif filename.endswith('.bz2'):
            cform = "bz2"
    # print("DEBUG: openCompressed(%s,%s,%s)" % (filename, mode, cform))
    k = kw.copy()
    if cform == "gzip":
        if "buffering" in k:
            k["bufsize"] = k["buffering"]
            del k["buffering"]
        return openGzip(filename, mode, **kw)
    elif cform == "bz2":
        k.setdefault("buffering",2**20)
        return bz2.open(filename, mode, **kw)
    elif cform == "none":
        k.setdefault("buffering",2**20)
        return open(filename, mode, **kw)
    else:
        raise ValueError(compressFormat)


def openGzip(filename, mode, compresslevel=6):
    compresslevel = int(compresslevel)

    if mode in ("w", "wb", "a", "ab"):
        if mode in ("w", "wb"):
            redir = ">"
        elif mode in ("a", "ab"):
            redir = ">>"
        # p = Popen("gzip -%s %s %s" % (compresslevel,redir,filename), shell=True, env=os.environ, bufsize=2**16, stdin=PIPE, close_fds=True)
        stdout = open(filename, mode)
#        p = Popen(cmd, env=os.environ, bufsize=2 ** 16, stdout=PIPE, close_fds=True, text=True)
        p = Popen(["gzip", "-%s" % compresslevel, "-c"], shell=False, env=os.environ,
                  bufsize=2**16, stdin=PIPE, stdout=stdout, close_fds=True, text=True)
        stdout.close()
        return PopenStdinProxy(p)

    elif mode in ("r", "rb", "rt"):
        cmd = ["gzip", "-cd", filename]
        if not os.path.isfile(filename):
            raise OSError("Input file does not exist: %s", filename)
        p = Popen(cmd, env=os.environ, bufsize=2**16, stdout=PIPE, close_fds=True, text=True)
        if p.returncode:
            raise CalledProcessError(str(cmd), p.returncode)
        return p.stdout

    else:
        raise ValueError("'openGzip()' - Unsupported 'mode': " + mode)
