#!/usr/bin/env python
# indexes lines of a text file for shard_reader.py
# Jeff Froula <jlfroula@lbl.gov>
# 09/04/2019
#
import sys,os
import optparse
from Bio.SeqIO.QualityIO import FastqGeneralIterator
from subprocess import Popen, call, PIPE
from jaws_sharding.helper_functions import openCompressed

uid=0
recordnum=0
seqLen=0
start=0

usage = "textfile_indexer.py -i <fastq> -o <outIndexFile>"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-i", "--input", dest="infilename",
                      action="store", type="string", help="input fastq file", default=False)
parser.add_option("-o", "--output", dest="outfilename",
                      action="store", type="string", help="output index file")
(options, args) = parser.parse_args()

if options.infilename == options.outfilename:
        print("Your output file should not be named same as input")
        sys.exit(1)

if not options.infilename or not options.outfilename:
	print("Missing arguments.\n{}".format(usage))
	sys.exit(1)

if not os.path.exists(options.infilename):
	parser.error("File doesn't seem to exist: {}".format(options.infilename))

if (os.path.getsize(options.infilename) < 1):
        print("File exists but is empty: {}".format(options.infilename))
        sys.exit(1)

f = open(options.outfilename, "w")

FH = openCompressed(options.infilename,"rt")
uid=0
for line in FH:
    l = line.strip()
    linelen = len(l)
    if linelen < 1:
        continue

    # "start" index starts from 0 and not 1. I also need
    # to count the + line and each \n
    end = start + linelen
    f.write("{}\t{}\t{}\t{}\n".format(start,end,linelen,uid))
    start=end+1
    uid += 1

f.close()
