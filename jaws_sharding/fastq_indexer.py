#!/usr/bin/env python
# indexes fastq reads for shard_reader.py
# Jeff Froula <jlfroula@lbl.gov>
# 08/21/2019
#
import sys,os
import optparse
from Bio.SeqIO.QualityIO import FastqGeneralIterator
from subprocess import Popen, call, PIPE
from jaws_sharding.helper_functions import openCompressed

uid=0
recordnum=0
seqLen=0
start=0

usage = "fasta_indexer.py -i <fastq> -o <outIndexFile>"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-i", "--input", dest="infilename",
                      action="store", type="string", help="input fastq file", default=False)
parser.add_option("-o", "--output", dest="outfilename",
                      action="store", type="string", help="output index file")
(options, args) = parser.parse_args()

if not options.infilename or not options.outfilename:
	print("Missing arguments.\n{}".format(usage))
	sys.exit(1)

if not os.path.exists(options.infilename):
	parser.error("File doesn't seem to exist: {}".format(options.infilename))

if (os.path.getsize(options.infilename) < 1):
        print("File exists but is empty: {}".format(options.infilename))
        sys.exit(1)



f = open(options.outfilename, "w")

#decompressed = writeCompressed(options.infilename, "rb")

#with open(options.infilename,"rt") as d:
for title, seq, qual in FastqGeneralIterator(openCompressed(options.infilename,"rt")):
    id_len = len(title)+1
    seq_len = len(seq)
    qual_len = len(qual)

    # "start" index starts from 0 and not 1. I also need
    # to count the + line and each \n
    end = start + id_len + seq_len + qual_len + 4
    f.write("{}\t{}\t{}\t{}\n".format(start,end,seq_len,uid))
    start=end+1
    uid+=1

f.close()
