#!/usr/bin/env python
# This script will detect if a fastq file is compressed with 
# bz2 or gz and decompress it to disk. It will change read names that may have 
# -R1 or -R2 endings instead of the prefered /1 & /2.
import sys
import re
from Bio.SeqIO.QualityIO import FastqGeneralIterator
from helper_functions import unCompressFile
import optparse

usage = "decompress_and_fix_headers.py -i <fastq> -o <decompressed & fixed fastq>"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-i", "--input", dest="infilename",
                      action="store", type="string", help="input fastq file", default=False)
parser.add_option("-o", "--output", dest="outfilename",
                      action="store", type="string", help="decompressed file with read headers -R1/-R2 changed to /1 & /2")
(options, args) = parser.parse_args()

if len(sys.argv) != 5:
    print(usage)
    sys.exit(1)

if options.infilename == options.outfilename:
    print("Your input and output file names are the same.  Please choose another output name.")
    sys.exit(1)

f = open(options.outfilename,"w")

# this function decompresses a bz2 or gz file and writes it to disk.
# the filename is returned
decompressed = unCompressFile(options.infilename, "rb")

with open(decompressed,"r") as d:
    for title, seq, qual in FastqGeneralIterator(d):
        header = re.match(r"(.*)-R([12])",title)
        new_header=''
        if header: 
            if header.group(2) == "1":
                new_header = "%s%s"%(header.group(1),"/1")
            elif header.group(2) == "2":
                new_header = "%s%s"%(header.group(1),"/2")
            else:
                print("error parsing fastq header.Expect 1 or 2 for read name and got %s "%header.group(2))
                sys.exit(1)
        else:
            print("header doesn't have -R1 or -R2 ending")
            sys.exit()
        
        f.write("@{}\n{}\n+\n{}\n".format(new_header,seq,qual))
    
f.close()
