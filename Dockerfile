FROM python:3.9-slim-buster
RUN apt-get update && apt-get install -y git
WORKDIR /usr/jaws-sharding
RUN git clone https://gitlab.com/jfroula/jaws-sharding.git ./src
RUN cd ./src && pip install -r requirements.txt && python setup.py install
CMD "/bin/bash"
